FROM maven:3.8.4-jdk-11
WORKDIR /src
COPY . .
RUN mvn clean install -Dmaven.test.skip
CMD mvn spring-boot:run