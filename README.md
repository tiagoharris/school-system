# school-system

This repository contains the implementation of a Rest API to manage course and students.

## requirements

```
School registration system

Design and implement simple school registration system
- Assuming you already have a list of students
- Assuming you already have a list of courses
- A student can register to multiple courses
- A course can have multiple students enrolled in it.
- A course has 2 students maximum
- A student can register to 5 course maximum

Provide the following REST API:
- Create students CRUD
- Create courses CRUD
- Create API for students to register to courses
- Create abilities for user to view all relationships between students and courses
+ Filter all students with a specific course
+ Filter all courses for a specific student
+ Filter all courses without any students
+ Filter all students without any courses
```

## technical decisions

1. I'm using [Spring Boot](https://spring.io/projects/spring-boot) 
2. I'm using [DTO](https://en.wikipedia.org/wiki/Data_transfer_object) to avoid exposing the entity directly. I explain it in my published article: [Spring Boot: an example of a CRUD RESTful API with global exception handling](https://www.linkedin.com/pulse/spring-boot-example-crud-restful-api-global-exception-tiago-melo/)
3. I'm using [Liquibase](http://www.liquibase.org/) for managing database migrations. Make sure you check my article about it: [Java: database versioning with Liquibase](https://www.linkedin.com/pulse/java-database-versioning-liquibase-tiago-melo/)
4. I'm using [JaCoCo](https://www.eclemma.org/jacoco/) to check for test coverage. I decided to not unit tests both controllers for the sake of time, but you can check [Spring Boot: an example of a CRUD RESTful API with global exception handling](https://www.linkedin.com/pulse/spring-boot-example-crud-restful-api-global-exception-tiago-melo/) to get a glance of how I do it
5. I'm using [springdoc.org](springdoc.org) to generate API documentation

## running it

### with docker-compose, for both app and databases (live database and test database)

```
$ docker-compose up
```

then the application will be available at `localhost:6868`.

### without docker-compose for app, but for the live database

in one terminal:

```
$ docker-compose up mysqldb
```

in another terminal:

```
$ mvn spring-boot:run
```

then the application will be available at `localhost:8080`.

## running unit tests

in one terminal:

```
$ docker-compose up mysqldb-test
```

in another terminal

```
$ mvn test && mvn jacoco:report
```

then you can check a report of test coverage in `metadata-school/target/site/jacoco/index.html`


## API documentation

### with docker-compose

```
$ docker-compose up
```

Then, you can point your browser to [http://localhost:6868/swagger-ui/index.html](http://localhost:6868/swagger-ui/index.html).

#### json description

[http://localhost:6868/v3/api-docs/](http://localhost:6868/v3/api-docs/)

#### yaml description

[http://localhost:6868/v3/api-docs.yaml](http://localhost:6868/v3/api-docs.yaml)


### without docker-compose

in one terminal:

```
$ docker-compose up mysqldb
```

in another terminal:

```
$ mvn spring-boot:run
```

Then, you can point your browser to [http://localhost:8080/swagger-ui/index.html](http://localhost:6868/swagger-ui/index.html).

#### json description

[http://localhost:6868/v3/api-docs/](http://localhost:8080/v3/api-docs/)

#### yaml description

[http://localhost:6868/v3/api-docs.yaml](http://localhost:8080/v3/api-docs.yaml)

## bonus

I've includes a json file that can be imported into [Postman](https://www.postman.com/) will all basic operations, as well as expected errors.

It's under `Postman` folder.

There are `dockerized` and `non-dockerized` folders in that collection, so you can choose.

