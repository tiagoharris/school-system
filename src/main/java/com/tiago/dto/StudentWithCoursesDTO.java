package com.tiago.dto;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTO for Student with courses information.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StudentWithCoursesDTO extends StudentDTO {

	private Set<String> courses;

	public Set<String> getCourses() {
		return courses;
	}

	public void setCourses(Set<String> courses) {
		this.courses = courses;
	}

}
