package com.tiago.dto;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTO for Student enrollment to courses.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StudentCoursesEnrollDTO {

	private Integer id;

	private Set<String> courses;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<String> getCourses() {
		return courses;
	}

	public void setCourses(Set<String> courses) {
		this.courses = courses;
	}
}
