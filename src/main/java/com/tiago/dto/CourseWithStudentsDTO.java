package com.tiago.dto;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTO for Course with students information.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CourseWithStudentsDTO extends CourseDTO {
	
	Set<String> students;

	public Set<String> getStudents() {
		return students;
	}

	public void setStudents(Set<String> students) {
		this.students = students;
	}
}
