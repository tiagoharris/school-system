package com.tiago.configuration;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class that makes possible to inject the beans listed here.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Configuration
public class MetadataSchoolApplicationConfiguration {

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		// This is to allow for partial updates
		modelMapper.getConfiguration()
			.setSkipNullEnabled(true)
			.setPropertyCondition(Conditions.isNotNull());

		return modelMapper;
	}
}
