package com.tiago.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tiago.dto.CourseDTO;
import com.tiago.dto.CourseWithStudentsDTO;
import com.tiago.service.CourseService;

/**
 * Restful controller responsible for managing courses
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@RestController
@RequestMapping("/api")
public class CourseController {

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	CourseService service;

	private static final Logger LOGGER = LoggerFactory.getLogger(CourseController.class);

	/**
	 * Get course with given id
	 * 
	 * @param courseId
	 * @return {@link ResponseEntity}<{@link CourseDTO}>
	 */
	@GetMapping("/course/{id}")
	public ResponseEntity<CourseDTO> getCourse(@PathVariable(value = "id") Integer courseId) {
		CourseDTO courseDTO = service.findById(courseId);

		LOGGER.info(String.format("getCourse() called"));

		return new ResponseEntity<>(courseDTO, HttpStatus.OK);
	}

	/**
	 * Get course with its registered students
	 * 
	 * @param courseId
	 * @return {@link ResponseEntity}<{@link CourseWithStudentsDTO}>
	 */
	@GetMapping("/course/{id}/students")
	public ResponseEntity<CourseWithStudentsDTO> getCourseWithStudents(
			@PathVariable(value = "id", required = true) Integer courseId) {
		CourseWithStudentsDTO courseWithStudentsDTO = service.findByIdWithStudents(courseId);

		LOGGER.info(String.format("getCourse() called"));

		return new ResponseEntity<>(courseWithStudentsDTO, HttpStatus.OK);
	}

	/**
	 * Get all courses
	 * 
	 * @return {@link ResponseEntity}<{@link List}<{@link CourseDTO}>>
	 */
	@GetMapping("/courses")
	public ResponseEntity<List<CourseDTO>> getAllCourses() {
		List<CourseDTO> coursesDTOs = service.findAll();

		LOGGER.info(String.format("getAllCourses() returned %s records", coursesDTOs.size()));

		return new ResponseEntity<>(coursesDTOs, HttpStatus.OK);
	}

	/**
	 * Get all courses without students
	 * 
	 * @return {@link ResponseEntity}<{@link List}<{@link CourseDTO}>>
	 */
	@GetMapping("/courses/withoutStudents")
	public ResponseEntity<List<CourseDTO>> getAllCoursesWithoutStudents() {
		List<CourseDTO> coursesDTOs = service.findAllWithoutStudents();

		LOGGER.info(String.format("getAllCourses() returned %s records", coursesDTOs.size()));

		return new ResponseEntity<>(coursesDTOs, HttpStatus.OK);
	}

	/**
	 * Creates a course
	 * 
	 * @param {@link CourseDTO}
	 * @return {@link ResponseEntity}<{@link CourseDTO}>
	 */
	@PostMapping("/course")
	public ResponseEntity<CourseDTO> createCourse(@Validated @RequestBody CourseDTO courseDTO) {
		LOGGER.info("createCourse() called");

		return new ResponseEntity<>(service.save(courseDTO), HttpStatus.CREATED);
	}

	/**
	 * Updates a course
	 * 
	 * @param courseId
	 * @param {@link CourseDTO}
	 * @return {@link ResponseEntity}<{@link CourseDTO}>
	 *
	 */
	@PutMapping("/course/{id}")
	public ResponseEntity<CourseDTO> updateCourse(@PathVariable(value = "id", required = true) Integer courseId,
			@Validated @RequestBody CourseDTO courseDTO) {
		courseDTO.setId(courseId);

		LOGGER.info(String.format("updateCourse() called with id [%s]", courseId));

		return new ResponseEntity<>(service.save(courseDTO), HttpStatus.OK);
	}

	/**
	 * Deletes a course
	 * 
	 * @param courseId
	 * @return 200 OK
	 */
	@DeleteMapping("/course/{id}")
	public ResponseEntity<?> deleteCourse(@PathVariable(value = "id") Integer courseId) {
		service.delete(courseId);

		LOGGER.info(String.format("deleteCourse() called with id [%s]", courseId));

		return ResponseEntity.ok().build();
	}
}
