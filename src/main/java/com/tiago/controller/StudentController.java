package com.tiago.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tiago.dto.StudentCoursesEnrollDTO;
import com.tiago.dto.StudentDTO;
import com.tiago.dto.StudentWithCoursesDTO;
import com.tiago.service.StudentService;

/**
 * Restful controller responsible for managing students
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@RestController
@RequestMapping("/api")
public class StudentController {

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	StudentService service;

	private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);

	/**
	 * Get student with given id
	 * 
	 * @return {@link ResponseEntity}<{@link StudentDTO}>
	 */
	@GetMapping("/student/{id}")
	public ResponseEntity<StudentDTO> getStudent(@PathVariable(value = "id", required = true) Integer studentId) {
		StudentDTO studentDTO = service.findById(studentId);

		LOGGER.info(String.format("getStudent() called"));

		return new ResponseEntity<>(studentDTO, HttpStatus.OK);
	}

	/**
	 * Get student with its enrolled courses
	 * 
	 * @param studentId
	 * @return {@link ResponseEntity}<{@link StudentWithCoursesDTO}>
	 */
	@GetMapping("/student/{id}/courses")
	public ResponseEntity<StudentWithCoursesDTO> getStudentWithCourses(
			@PathVariable(value = "id", required = true) Integer studentId) {
		StudentWithCoursesDTO studentWithCoursesDTO = service.findByIdWithCourses(studentId);

		LOGGER.info(String.format("getStudent() called"));

		return new ResponseEntity<>(studentWithCoursesDTO, HttpStatus.OK);
	}

	/**
	 * Get all students
	 * 
	 * @return {@link ResponseEntity}<{@link List}<{@link StudentDTO}>>
	 */
	@GetMapping("/students")
	public ResponseEntity<List<StudentDTO>> getAllStudents() {
		List<StudentDTO> studentsDTOs = service.findAll();

		LOGGER.info(String.format("getAllStudents() returned %s records", studentsDTOs.size()));

		return new ResponseEntity<>(studentsDTOs, HttpStatus.OK);
	}

	/**
	 * Get all students that are not enrolled to any course
	 * 
	 * @return {@link ResponseEntity}<{@link List}<{@link StudentDTO}>>
	 */
	@GetMapping("/students/notEnrolledToAnyCourse")
	public ResponseEntity<List<StudentDTO>> getAllStudentsNotEnrolledToAnyCourse() {
		List<StudentDTO> studentsDTOs = service.findAllNotEnrolledToAnyCourse();

		LOGGER.info(String.format("getAllStudents() returned %s records", studentsDTOs.size()));

		return new ResponseEntity<>(studentsDTOs, HttpStatus.OK);
	}

	/**
	 * Creates a student
	 * 
	 * @param {@link StudentDTO}
	 * @return {@link ResponseEntity}<{@link StudentDTO}>
	 */
	@PostMapping("/student")
	public ResponseEntity<StudentDTO> createStudent(@Validated @RequestBody StudentDTO studentDTO) {
		LOGGER.info("createStudent() called");

		return new ResponseEntity<>(service.save(studentDTO), HttpStatus.CREATED);
	}

	/**
	 * Updates a student
	 * 
	 * @param studentId
	 * @param {@link    StudentDTO}
	 * @return {@link ResponseEntity}<{@link StudentDTO}>
	 */
	@PutMapping("/student/{id}")
	public ResponseEntity<StudentDTO> updateStudent(@PathVariable(value = "id", required = true) Integer studentId,
			@Validated @RequestBody StudentDTO studentDTO) {
		studentDTO.setId(studentId);

		LOGGER.info(String.format("updateStudent() called with id [%s]", studentId));

		return new ResponseEntity<>(service.save(studentDTO), HttpStatus.OK);
	}

	/**
	 * Enrolls a student to courses
	 * 
	 * @param studentId
	 * @param {@link    studentCoursesEnrollDTO}
	 * @return {@link ResponseEntity}<{@link StudentDTO}>
	 */
	@PutMapping("/student/{id}/courses")
	public ResponseEntity<StudentDTO> enrollStudentToCourses(
			@PathVariable(value = "id", required = true) Integer studentId,
			@Validated @RequestBody StudentCoursesEnrollDTO studentCoursesEnrollDTO) {
		studentCoursesEnrollDTO.setId(studentId);

		LOGGER.info(String.format("enrollStudentToCourses() called with id [%s]", studentId));

		return new ResponseEntity<>(service.enrollToCourses(studentCoursesEnrollDTO), HttpStatus.OK);
	}

	/**
	 * Removes a students from courses
	 * 
	 * @param studentId
	 * @param {@link    StudentCoursesEnrollDTO}
	 * @return {@link ResponseEntity}<{@link StudentDTO}>
	 */
	@DeleteMapping("/student/{id}/courses")
	public ResponseEntity<StudentDTO> removeStudentFromCourses(
			@PathVariable(value = "id", required = true) Integer studentId,
			@Validated @RequestBody StudentCoursesEnrollDTO studentCoursesEnrollDTO) {
		studentCoursesEnrollDTO.setId(studentId);

		LOGGER.info(String.format("enrollStudentToCourses() called with id [%s]", studentId));

		return new ResponseEntity<>(service.removeFromCourses(studentCoursesEnrollDTO), HttpStatus.OK);
	}

	/**
	 * Deletes a student
	 * 
	 * @param studentId
	 * @return 200 OK
	 */
	@DeleteMapping("/student/{id}")
	public ResponseEntity<?> deleteStudent(@PathVariable(value = "id") Integer studentId) {
		service.delete(studentId);

		LOGGER.info(String.format("deleteStudent() called with id [%s]", studentId));

		return ResponseEntity.ok().build();
	}
}
