package com.tiago.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.tiago.dto.StudentCoursesEnrollDTO;
import com.tiago.dto.StudentDTO;
import com.tiago.dto.StudentWithCoursesDTO;
import com.tiago.entity.Course;
import com.tiago.entity.Student;
import com.tiago.exception.MaximumEnrolledCoursesReachedException;
import com.tiago.exception.MaximumEnrolledStudentsReachedException;
import com.tiago.exception.ResourceNotFoundException;
import com.tiago.repository.CourseRepository;
import com.tiago.repository.StudentRepository;
import com.tiago.service.StudentService;

/**
 * Implements {@link StudentService} interface
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepository repository;

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	ModelMapper modelMapper;

	@Value("${students.max.allowed.courses}")
	private int maxAllowedCourses;

	@Value("${courses.max.allowed.students}")
	private int maxAllowedSudents;

	@Override
	public StudentDTO findById(Integer id) {
		return mapEntityToStudentDTO(findStudentById(id));
	}

	@Override
	public StudentWithCoursesDTO findByIdWithCourses(Integer id) {
		return mapEntityToStudentWithCoursesDTO(findStudentById(id));
	}

	@Override
	public List<StudentDTO> findAll() {
		List<StudentDTO> studentDTOs = new ArrayList<>();
		List<Student> students = repository.findAll();
		students.stream().forEach(student -> {
			StudentDTO studentDto = mapEntityToStudentDTO(student);
			studentDTOs.add(studentDto);
		});
		return studentDTOs;
	}

	@Override
	public List<StudentDTO> findAllNotEnrolledToAnyCourse() {
		List<StudentDTO> studentDTOs = new ArrayList<>();
		List<Student> students = repository.findAllNotEnrolledToAnyCourse();
		students.stream().forEach(student -> {
			StudentDTO studentDto = mapEntityToStudentDTO(student);
			studentDTOs.add(studentDto);
		});
		return studentDTOs;
	}

	@Override
	public StudentDTO save(StudentDTO studentDTO) {
		Student student = convertToEntity(studentDTO);
		return mapEntityToStudentDTO(repository.save(student));
	}

	@Override
	public StudentWithCoursesDTO enrollToCourses(StudentCoursesEnrollDTO studentCoursesEnrollDTO) {
		Student student = findStudentById(studentCoursesEnrollDTO.getId());
		Set<Course> studentCourses = student.getCourses();
		if (studentCourses.isEmpty()) {
			if (studentCourses.size() > maxAllowedCourses
					|| studentCoursesEnrollDTO.getCourses().size() > maxAllowedCourses) {
				throw new MaximumEnrolledCoursesReachedException(Student.class.getSimpleName(), "name",
						student.getName(), maxAllowedCourses);
			}
		}
		if (studentCourses.size() >= maxAllowedCourses
				|| studentCoursesEnrollDTO.getCourses().size() > maxAllowedCourses) {
			throw new MaximumEnrolledCoursesReachedException(Student.class.getSimpleName(), "name", student.getName(),
					maxAllowedCourses);
		}
		studentCoursesEnrollDTO.getCourses().stream().forEach(courseName -> {
			Course course = courseRepository.findByName(courseName);
			if (null == course) {
				throw new ResourceNotFoundException(Course.class.getSimpleName(), "name", courseName);
			}
			Set<Student> courseStudents = course.getStudents();
			if (courseStudents.isEmpty()) {
				if (courseStudents.size() > maxAllowedSudents) {
					throw new MaximumEnrolledStudentsReachedException(Course.class.getSimpleName(), "name",
							course.getName(), maxAllowedSudents);
				}
			}
			if (courseStudents.size() >= maxAllowedSudents) {
				throw new MaximumEnrolledStudentsReachedException(Course.class.getSimpleName(), "name",
						course.getName(), maxAllowedSudents);
			}
			course.setName(courseName);
			student.addCourse(course);
		});
		return mapEntityToStudentWithCoursesDTO(repository.save(student));
	}

	@Override
	public StudentWithCoursesDTO removeFromCourses(StudentCoursesEnrollDTO studentCoursesEnrollDTO) {
		Student student = findStudentById(studentCoursesEnrollDTO.getId());
		studentCoursesEnrollDTO.getCourses().stream().forEach(courseName -> {
			Course course = courseRepository.findByName(courseName);
			if (null == course) {
				throw new ResourceNotFoundException(Course.class.getSimpleName(), "name", courseName);
			}
			course.setName(courseName);
			student.removeCourse(course);
		});
		return mapEntityToStudentWithCoursesDTO(repository.save(student));
	}

	@Override
	public void delete(Integer id) {
		StudentDTO studentDTO = findById(id);

		repository.delete(convertToEntity(studentDTO));
	}

	private Student findStudentById(Integer id) {
		Student student = repository.findById(id).orElse(null);

		if (student == null) {
			throw new ResourceNotFoundException(Student.class.getSimpleName(), "id", id);
		}

		return student;
	}

	private StudentDTO mapEntityToStudentDTO(Student student) {
		return modelMapper.map(student, StudentDTO.class);
	}

	private StudentWithCoursesDTO mapEntityToStudentWithCoursesDTO(Student student) {
		StudentWithCoursesDTO studentWithCoursesDTO = new StudentWithCoursesDTO();
		modelMapper.map(student, studentWithCoursesDTO);
		studentWithCoursesDTO
				.setCourses(student.getCourses().stream().map(Course::getName).collect(Collectors.toSet()));
		return studentWithCoursesDTO;
	}

	private Student convertToEntity(StudentDTO studentDTO) {
		Student student = null;

		if (studentDTO.getId() != null) {
			student = repository.findById(studentDTO.getId()).orElse(null);
			if (student == null) {
				throw new ResourceNotFoundException(Course.class.getSimpleName(), "id", studentDTO.getId());
			}

		} else {
			student = new Student();
		}

		modelMapper.map(studentDTO, student);

		return student;
	}
}
