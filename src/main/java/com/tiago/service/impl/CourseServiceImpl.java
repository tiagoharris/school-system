package com.tiago.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tiago.dto.CourseDTO;
import com.tiago.dto.CourseWithStudentsDTO;
import com.tiago.entity.Course;
import com.tiago.entity.Student;
import com.tiago.exception.ResourceNotFoundException;
import com.tiago.repository.CourseRepository;
import com.tiago.service.CourseService;

/**
 * Implements {@link CourseService} interface
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Service
public class CourseServiceImpl implements CourseService {

	@Autowired
	CourseRepository repository;

	@Autowired
	ModelMapper modelMapper;

	@Override
	public CourseDTO findById(Integer id) {
		return mapEntityToCourseDTO(findCourseById(id));
	}

	@Override
	public CourseWithStudentsDTO findByIdWithStudents(Integer id) {
		return mapEntityToCourseWithStudentsDTO(findCourseById(id));
	}

	@Override
	public List<CourseDTO> findAll() {
		List<CourseDTO> courseDTOs = new ArrayList<>();
		List<Course> courses = repository.findAll();
		courses.stream().forEach(course -> {
			CourseDTO courseDto = mapEntityToCourseDTO(course);
			courseDTOs.add(courseDto);
		});
		return courseDTOs;
	}

	@Override
	public List<CourseDTO> findAllWithoutStudents() {
		List<CourseDTO> courseDTOs = new ArrayList<>();
		List<Course> courses = repository.findAllWithoutStudents();
		courses.stream().forEach(course -> {
			CourseDTO courseDto = mapEntityToCourseDTO(course);
			courseDTOs.add(courseDto);
		});
		return courseDTOs;
	}

	@Override
	public CourseDTO save(CourseDTO courseDTO) {
		Course course = convertToEntity(courseDTO);
		return mapEntityToCourseDTO(repository.save(course));
	}

	@Override
	public void delete(Integer id) {
		CourseDTO courseDTO = findById(id);

		repository.delete(convertToEntity(courseDTO));
	}

	private CourseDTO mapEntityToCourseDTO(Course course) {
		return modelMapper.map(course, CourseDTO.class);
	}

	private CourseWithStudentsDTO mapEntityToCourseWithStudentsDTO(Course course) {
		CourseWithStudentsDTO courseWithStudentsDTO = new CourseWithStudentsDTO();

		modelMapper.map(course, courseWithStudentsDTO);

		courseWithStudentsDTO
				.setStudents(course.getStudents().stream().map(Student::getName).collect(Collectors.toSet()));

		return courseWithStudentsDTO;
	}

	private Course findCourseById(Integer id) {
		Course course = repository.findById(id).orElse(null);

		if (course == null) {
			throw new ResourceNotFoundException(Course.class.getSimpleName(), "id", id);
		}

		return course;
	}

	private Course convertToEntity(CourseDTO courseDTO) {
		Course course = null;

		if (courseDTO.getId() != null) {
			course = repository.findById(courseDTO.getId()).orElse(null);
			if (course == null) {
				throw new ResourceNotFoundException(Course.class.getSimpleName(), "id", courseDTO.getId());
			}
		} else {
			course = new Course();
		}

		modelMapper.map(courseDTO, course);

		return course;
	}
}
