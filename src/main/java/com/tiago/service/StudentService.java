package com.tiago.service;

import java.util.List;

import com.tiago.dto.StudentCoursesEnrollDTO;
import com.tiago.dto.StudentDTO;
import com.tiago.dto.StudentWithCoursesDTO;
import com.tiago.entity.Student;

/**
 * Service to manage students.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public interface StudentService {

	/**
	 * Finds a student by id
	 * 
	 * @param id
	 * @return {@link Student}
	 */
	StudentDTO findById(Integer id);

	/**
	 * Find a student by id with its courses
	 * 
	 * @param id
	 * @return {@link StudentWithCoursesDTO}
	 */
	StudentWithCoursesDTO findByIdWithCourses(Integer id);

	/**
	 * Find all students
	 * 
	 * @return {@link List}<{@link StudentDTO}>
	 */
	List<StudentDTO> findAll();

	/**
	 * Find all students that are not enrolled to any course
	 * 
	 * @return {@link List}<{@link StudentDTO}>
	 */
	List<StudentDTO> findAllNotEnrolledToAnyCourse();

	/**
	 * Saves a student
	 * 
	 * @param {@link StudentDTO}
	 * @return {@link StudentDTO}
	 */
	StudentDTO save(StudentDTO studentDTO);

	/**
	 * Enrolls a student to given course names
	 * 
	 * @param {@link StudentCoursesEnrollDTO}
	 * @return {@link StudentWithCoursesDTO}
	 */
	StudentWithCoursesDTO enrollToCourses(StudentCoursesEnrollDTO studentCoursesEnrollDTO);

	/**
	 * Removes a student from given course names
	 * 
	 * @param {@link StudentCoursesEnrollDTO}
	 * @return {@link StudentWithCoursesDTO}
	 */
	StudentWithCoursesDTO removeFromCourses(StudentCoursesEnrollDTO studentCoursesEnrollDTO);

	/**
	 * Deletes a student
	 * 
	 * @param id
	 */
	void delete(Integer id);
}
