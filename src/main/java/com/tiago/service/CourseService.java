package com.tiago.service;

import java.util.List;

import com.tiago.dto.CourseDTO;
import com.tiago.dto.CourseWithStudentsDTO;

/**
 * Service to manage courses.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public interface CourseService {

	/**
	 * Finds a Course by id
	 * 
	 * @param id
	 * @return {@link CourseDTO}
	 */
	CourseDTO findById(Integer id);

	/**
	 * Finds a course by id and return its students
	 * 
	 * @param id
	 * @return {@link CourseWithStudentsDTO}
	 */
	CourseWithStudentsDTO findByIdWithStudents(Integer id);

	/**
	 * Find all Courses
	 * 
	 * @return the list of {@link CourseDTO}
	 */
	List<CourseDTO> findAll();

	/**
	 * Find all courses without students
	 * 
	 * @return {@link List}<{@link CourseDTO}>
	 */
	List<CourseDTO> findAllWithoutStudents();

	/**
	 * Saves a Course
	 * 
	 * @param {@link CourseDTO}
	 * @return {@link CourseDTO}
	 */
	CourseDTO save(CourseDTO courseDTO);

	/**
	 * Deletes a Course
	 * 
	 * @param id
	 */
	void delete(Integer id);
}
