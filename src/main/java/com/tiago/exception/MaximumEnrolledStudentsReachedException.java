package com.tiago.exception;

/**
 * Exception class that is thrown when a course reaches its limit of enrolled
 * students.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class MaximumEnrolledStudentsReachedException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MaximumEnrolledStudentsReachedException(String resourceName, String fieldName, Object fieldValue,
			int maxStudentsLimit) {
		super(String.format("%s with %s '%s' reached maximum enrolled students limit of %d", resourceName, fieldName,
				fieldValue, maxStudentsLimit));
	}
}
