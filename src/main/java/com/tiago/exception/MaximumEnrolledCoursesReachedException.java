package com.tiago.exception;

/**
 * Exception class that is thrown when a student tries to enroll to more coures
 * than the limit.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class MaximumEnrolledCoursesReachedException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MaximumEnrolledCoursesReachedException(String resourceName, String fieldName, Object fieldValue,
			int maxCoursesLimit) {
		super(String.format("%s with %s '%s' reached maximum enrolled courses limit of %d", resourceName, fieldName,
				fieldValue, maxCoursesLimit));
	}
}
