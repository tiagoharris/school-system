package com.tiago.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tiago.entity.Course;

/**
 * Repository for {@link Course} entity.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
*/
@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {
	
	/**
	 * Finds a course by its name.
	 * 
	 * @param courseName
	 * @return {@link Course}
	 */
	public Course findByName(String courseName);

	
	/**
	 * Return all courses without students.
	 * 
	 * @return {@link List}<{@link Course}>
	 */
	@Query(value = "SELECT c.* FROM courses c WHERE NOT EXISTS (SELECT * FROM students_courses sc WHERE sc.course_id = c.id)",
			nativeQuery = true)
	public List<Course> findAllWithoutStudents();
}
