package com.tiago.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tiago.entity.Student;

/**
 * Repository for {@link Student} entity.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
*/
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
	
	/**
	 * Returns all students that are not enrolled to any course.
	 * 
	 * @return {@link List}<{@link Student}>
	 */
	@Query(value = "SELECT s.* FROM students s WHERE NOT EXISTS (SELECT * FROM students_courses sc WHERE sc.student_id = s.id)",
			nativeQuery = true)
	public List<Student> findAllNotEnrolledToAnyCourse();

}
