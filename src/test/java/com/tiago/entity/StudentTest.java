package com.tiago.entity;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class StudentTest {

	@Test
	public void whenTwoStudentObjectsAreTheSameObject_thenReturnsTrue() {
		Student studentOne = buildStudent();
		Student studentTwo = studentOne;

		assertTrue(studentOne.equals(studentTwo));
		assertTrue(studentOne.hashCode() == studentTwo.hashCode());
	}

	@Test
	public void whenTwoStudentObjectsHaveTheSameId_thenReturnsTrue() {
		Student studentOne = new Student();
		studentOne.setId(1);

		Student studentTwo = new Student();
		studentTwo.setId(1);

		assertTrue(studentOne.equals(studentTwo));
		assertTrue(studentOne.hashCode() == studentTwo.hashCode());
	}

	@Test
	public void whenTwoStudentObjectsHaveSameProperties_thenReturnsTrue() {
		Student studentOne = buildStudent();
		Student studentTwo = buildStudent();

		assertTrue(studentOne.equals(studentTwo));
		assertTrue(studentOne.hashCode() == studentTwo.hashCode());
	}

	public void whenTwoStudentObjectsHaveDifferentIds_thenReturnsFalse() {
		Student studentOne = buildStudent();
		Student studentTwo = new Student();
		studentTwo.setId(2);
		studentTwo.setName(studentOne.getName());
		studentTwo.setEmail(studentOne.getEmail());

		assertFalse(studentOne.equals(studentTwo));
		assertFalse(studentOne.hashCode() == studentTwo.hashCode());
	}

	@Test
	public void whenTwoStudentObjectsHaveDifferentNames_thenReturnsFalse() {
		Student studentOne = buildStudent();
		Student studentTwo = new Student();
		studentTwo.setId(studentOne.getId());
		studentTwo.setName("other tiago");
		studentTwo.setEmail(studentOne.getEmail());

		assertFalse(studentOne.equals(studentTwo));
		assertFalse(studentOne.hashCode() == studentTwo.hashCode());
	}

	@Test
	public void whenTwoStudentObjectsHaveDifferentEmails_thenReturnsFalse() {
		Student studentOne = buildStudent();
		Student studentTwo = new Student();
		studentTwo.setId(studentOne.getId());
		studentTwo.setName(studentOne.getName());
		studentTwo.setEmail("other_email@email.com");

		assertFalse(studentOne.equals(studentTwo));
		assertFalse(studentOne.hashCode() == studentTwo.hashCode());
	}

	@Test
	public void whenComparingToNull_thenReturnsFalse() {
		Student student = buildStudent();
		assertFalse(student.equals(null));
	}

	@Test
	public void whenComparedToAnotherRandomObject_thenReturnsFalse() {
		Student student = buildStudent();
		assertFalse(student.equals(new Object()));
	}

	private Student buildStudent() {
		Student student = new Student();
		student.setId(1);
		student.setName("tiago");
		student.setEmail("tiago@email.com");

		return student;
	}
}
