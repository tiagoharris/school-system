package com.tiago.entity;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class CourseTest {

	@Test
	public void whenTwoCourseObjectsAreTheSameObject_thenReturnsTrue() {
		Course courseOne = buildCourse();
		Course courseTwo = courseOne;

		assertTrue(courseOne.equals(courseTwo));
		assertTrue(courseOne.hashCode() == courseTwo.hashCode());
	}

	@Test
	public void whenTwoCourseObjectsHaveTheSameId_thenReturnsTrue() {
		Course courseOne = new Course();
		courseOne.setId(1);

		Course courseTwo = new Course();
		courseTwo.setId(1);

		assertTrue(courseOne.equals(courseTwo));
		assertTrue(courseOne.hashCode() == courseTwo.hashCode());
	}

	@Test
	public void whenTwoCourseObjectsHaveSameProperties_thenReturnsTrue() {
		Course courseOne = buildCourse();
		Course courseTwo = buildCourse();

		assertTrue(courseOne.equals(courseTwo));
		assertTrue(courseOne.hashCode() == courseTwo.hashCode());
	}

	public void whenTwoCourseObjectsHaveDifferentIds_thenReturnsFalse() {
		Course courseOne = buildCourse();
		Course courseTwo = new Course();
		courseTwo.setId(2);
		courseTwo.setName(courseOne.getName());

		assertFalse(courseOne.equals(courseTwo));
		assertFalse(courseOne.hashCode() == courseTwo.hashCode());
	}

	@Test
	public void whenTwoCourseObjectsHaveDifferentNames_thenReturnsFalse() {
		Course courseOne = buildCourse();
		Course courseTwo = new Course();
		courseTwo.setId(courseOne.getId());
		courseTwo.setName("other course");

		assertFalse(courseOne.equals(courseTwo));
		assertFalse(courseOne.hashCode() == courseTwo.hashCode());
	}

	@Test
	public void whenComparingToNull_thenReturnsFalse() {
		Course course = buildCourse();
		assertFalse(course.equals(null));
	}

	@Test
	public void whenComparedToAnotherRandomObject_thenReturnsFalse() {
		Course course = buildCourse();
		assertFalse(course.equals(new Object()));
	}

	private Course buildCourse() {
		Course course = new Course();
		course.setId(1);
		course.setName("course");

		return course;
	}
}
