package com.tiago.service.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tiago.dto.StudentCoursesEnrollDTO;
import com.tiago.dto.StudentDTO;
import com.tiago.dto.StudentWithCoursesDTO;
import com.tiago.entity.Course;
import com.tiago.entity.Student;
import com.tiago.exception.MaximumEnrolledCoursesReachedException;
import com.tiago.exception.MaximumEnrolledStudentsReachedException;
import com.tiago.exception.ResourceNotFoundException;
import com.tiago.repository.CourseRepository;
import com.tiago.repository.StudentRepository;
import com.tiago.service.StudentService;

@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
@ContextConfiguration(classes = { StudentServiceImpl.class, ModelMapper.class })
public class StudentServiceImplTest {

	@Autowired
	private StudentService service;

	@MockBean
	private StudentRepository studentRepository;

	@MockBean
	private CourseRepository courseRepository;

	private static final Integer STUDENT_ID = 1;
	
	private static final Integer INVALID_STUDENT_ID = 8888;

	private static final Integer STUDENT_ID_WITH_COURSES = 2;

	private static final Integer STUDENT_ID_MAXCOURSESREACHED = 3;

	private static final String INVALID_COURSE_NAME = "invalid course";

	private static final String COURSE_NAME = "some course";

	private static final String FULL_COURSE_NAME = "full course";

	@BeforeEach
	public void setUp() {
		Mockito.when(studentRepository.findById(STUDENT_ID)).thenReturn(buildStudent());

		Mockito.when(studentRepository.findAllNotEnrolledToAnyCourse()).thenReturn(buildAllNotEnrolledStudentList());

		Mockito.when(studentRepository.findById(STUDENT_ID_WITH_COURSES)).thenReturn(buildStudentWithCourse());

		Mockito.when(studentRepository.findById(STUDENT_ID_MAXCOURSESREACHED))
				.thenReturn(buildStudentWithALotOfCourses());

		Mockito.when(studentRepository.findAll()).thenReturn(buildAllNotEnrolledStudentList());

		Mockito.when(studentRepository.save(any())).thenReturn(buildStudent().get());

		Mockito.when(courseRepository.findByName(INVALID_COURSE_NAME)).thenReturn(null);

		Mockito.when(courseRepository.findByName(FULL_COURSE_NAME)).thenReturn(buildFullCourse());

		Mockito.when(courseRepository.findByName(COURSE_NAME)).thenReturn(buildCourse());

	}

	@Test
	public void findById_whenNonExistingId_thenRaisesResourceNotFoundException() {
		Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
			service.findById(INVALID_STUDENT_ID);
		});

		String expectedMessage = "Student not found with id: '8888'";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	public void findById_happyPath() {
		StudentDTO studentDTO = service.findById(STUDENT_ID);
		assertNotNull(studentDTO);
	}

	@Test
	public void findByIdWithCourses_whenNonExistingId_thenRaisesResourceNotFoundException() {
		Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
			service.findByIdWithCourses(8888);
		});

		String expectedMessage = "Student not found with id: '8888'";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	public void save_returnsNewStudent() {
		StudentDTO newStudentDTO = new StudentDTO();
		newStudentDTO.setName("Tiago");
		newStudentDTO.setEmail("tiago@email.com");

		StudentDTO savedStudentDTO = service.save(newStudentDTO);

		assertEquals(newStudentDTO.getName(), savedStudentDTO.getName());
		assertEquals(newStudentDTO.getEmail(), savedStudentDTO.getEmail());
	}

	@Test
	public void findByIdWithCourses_whenExistingId_thenReturnStudent() {
		StudentDTO studentDTO = service.findByIdWithCourses(STUDENT_ID_WITH_COURSES);
		assertNotNull(studentDTO);
	}

	@Test
	public void findAll_returnAllStudents() {
		List<StudentDTO> studentDTOs = service.findAll();
		assertNotNull(studentDTOs);
		assertEquals(2, studentDTOs.size());
	}

	@Test
	public void findAllNotEnrolledToAnyCourse_returnStudents() {
		List<StudentDTO> studentDTOs = service.findAllNotEnrolledToAnyCourse();
		assertNotNull(studentDTOs);
		assertEquals(2, studentDTOs.size());
	}

	@Test
	public void enrollToCourses_whenMaxAllowedCoursesReached_thenRaisesMaximumEnrolledCoursesReachedException() {
		Exception exception = assertThrows(MaximumEnrolledCoursesReachedException.class, () -> {
			StudentCoursesEnrollDTO studentCoursesEnrollDTO = new StudentCoursesEnrollDTO();
			studentCoursesEnrollDTO.setId(STUDENT_ID_MAXCOURSESREACHED);
			service.enrollToCourses(studentCoursesEnrollDTO);
		});

		String expectedMessage = "Student with name 'Tiago' reached maximum enrolled courses limit of 3";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	public void enrollToCourses_whenAddingMaxAllowedCoursesReached_thenRaisesMaximumEnrolledCoursesReachedException() {
		Exception exception = assertThrows(MaximumEnrolledCoursesReachedException.class, () -> {
			StudentCoursesEnrollDTO studentCoursesEnrollDTO = new StudentCoursesEnrollDTO();
			studentCoursesEnrollDTO.setId(STUDENT_ID_MAXCOURSESREACHED);
			Set<String> courseNames = new HashSet<String>();
			for (int i = 0; i < 10; i++) {
				courseNames.add("course " + i);
			}
			studentCoursesEnrollDTO.setCourses(courseNames);
			service.enrollToCourses(studentCoursesEnrollDTO);
		});

		String expectedMessage = "Student with name 'Tiago' reached maximum enrolled courses limit of 3";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	public void enrollToCourses_whenAddingInvalidCourse_thenRaisesResourceNotFoundException() {
		Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
			StudentCoursesEnrollDTO studentCoursesEnrollDTO = new StudentCoursesEnrollDTO();
			studentCoursesEnrollDTO.setId(STUDENT_ID);
			Set<String> courseNames = new HashSet<String>();
			courseNames.add(INVALID_COURSE_NAME);
			studentCoursesEnrollDTO.setCourses(courseNames);
			service.enrollToCourses(studentCoursesEnrollDTO);
		});

		String expectedMessage = "Course not found with name: 'invalid course'";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	public void enrollToCourses_whenMaxAllowedStudentsReached_thenRaisesMaximumEnrolledStudentsReachedException() {
		Exception exception = assertThrows(MaximumEnrolledStudentsReachedException.class, () -> {
			StudentCoursesEnrollDTO studentCoursesEnrollDTO = new StudentCoursesEnrollDTO();
			studentCoursesEnrollDTO.setId(STUDENT_ID);
			Set<String> courseNames = new HashSet<String>();
			courseNames.add(FULL_COURSE_NAME);
			studentCoursesEnrollDTO.setCourses(courseNames);
			service.enrollToCourses(studentCoursesEnrollDTO);
		});

		String expectedMessage = "Course with name 'full course' reached maximum enrolled students limit of 2";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	public void enrollToCourses_happyPath() {
		StudentCoursesEnrollDTO studentCoursesEnrollDTO = new StudentCoursesEnrollDTO();
		studentCoursesEnrollDTO.setId(STUDENT_ID);
		Set<String> courseNames = new HashSet<String>();
		courseNames.add(COURSE_NAME);
		studentCoursesEnrollDTO.setCourses(courseNames);
		StudentWithCoursesDTO studentWithCoursesDTO = service.enrollToCourses(studentCoursesEnrollDTO);

		assertNotNull(studentWithCoursesDTO);
	}

	@Test
	public void removeFromCourses_whenRemovingInvalidCourse_thenRaisesResourceNotFoundException() {
		Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
			StudentCoursesEnrollDTO studentCoursesEnrollDTO = new StudentCoursesEnrollDTO();
			studentCoursesEnrollDTO.setId(STUDENT_ID_WITH_COURSES);
			Set<String> courseNames = new HashSet<String>();
			courseNames.add(INVALID_COURSE_NAME);
			studentCoursesEnrollDTO.setCourses(courseNames);
			service.removeFromCourses(studentCoursesEnrollDTO);
		});

		String expectedMessage = "Course not found with name: 'invalid course'";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	public void removeFromCourses_happyPath() {
		StudentCoursesEnrollDTO studentCoursesEnrollDTO = new StudentCoursesEnrollDTO();
		studentCoursesEnrollDTO.setId(STUDENT_ID_WITH_COURSES);
		Set<String> courseNames = new HashSet<String>();
		courseNames.add(COURSE_NAME);
		studentCoursesEnrollDTO.setCourses(courseNames);
		
		StudentWithCoursesDTO studentWithCoursesDTO = service.removeFromCourses(studentCoursesEnrollDTO);
		assertEquals(studentWithCoursesDTO.getCourses().size(), 0);
	}
	
	@Test
	public void delete_whenDeletingInvalidStudent_thenRaisesResourceNotFoundException() {
		Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
			service.delete(INVALID_STUDENT_ID);
		});

		String expectedMessage = "Student not found with id: '8888'";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void delete_happyPath() {
		assertDoesNotThrow(() -> {
			service.delete(STUDENT_ID);
		});
	}

	private Optional<Student> buildStudent() {
		Student student = new Student();

		student.setId(STUDENT_ID);
		student.setName("Tiago");
		student.setEmail("tiago@email.com");
		Set<Course> courses = new HashSet<Course>();
		student.setCourses(courses);

		return Optional.of(student);
	}

	private Course buildCourse() {
		Course course = new Course();
		course.setId(1);
		course.setName(COURSE_NAME);
		course.setStudents(new HashSet<Student>());
		return course;
	}

	private Optional<Student> buildStudentWithCourse() {
		Student student = buildStudent().get();

		student.setId(STUDENT_ID_WITH_COURSES);
		student.setName("Tiago");
		student.setEmail("tiago@email.com");

		Set<Course> courses = new HashSet<Course>();
		courses.add(buildCourse());
		student.setCourses(courses);

		return Optional.of(student);
	}

	private Optional<Student> buildStudentWithALotOfCourses() {
		Student student = buildStudentWithCourse().get();
		student.setId(STUDENT_ID_MAXCOURSESREACHED);
		Set<Student> courseStudents = new HashSet<Student>();

		for (int i = 0; i < 10; i++) {
			Course course = new Course();
			course.setId(i);
			courseStudents.add(student);
			course.setStudents(courseStudents);

			student.addCourse(course);
		}

		return Optional.of(student);
	}

	private List<Student> buildAllNotEnrolledStudentList() {
		Student studentOne = new Student();

		studentOne.setId(2);
		studentOne.setName("Tiago");
		studentOne.setEmail("tiago@email.com");

		Student studentTwo = new Student();

		studentTwo.setId(3);
		studentTwo.setName("Tiago 2");
		studentTwo.setEmail("tiago2@email.com");

		List<Student> students = new ArrayList<Student>();
		students.add(studentOne);
		students.add(studentTwo);

		return students;
	}

	private Course buildFullCourse() {
		Course course = new Course();
		Set<Student> courseStudents = new HashSet<Student>();

		for (int i = 0; i < 10; i++) {
			Student student = new Student();
			student.setId(i);
			courseStudents.add(student);
		}

		course.setName(FULL_COURSE_NAME);
		course.setStudents(courseStudents);

		return course;
	}
}
