package com.tiago.service.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.tiago.dto.CourseDTO;
import com.tiago.dto.CourseWithStudentsDTO;
import com.tiago.entity.Course;
import com.tiago.entity.Student;
import com.tiago.exception.ResourceNotFoundException;
import com.tiago.repository.CourseRepository;
import com.tiago.service.CourseService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { CourseServiceImpl.class, ModelMapper.class })
public class CourseServiceImplTest {

	@Autowired
	private CourseService service;
	
	@MockBean
	private CourseRepository courseRepository;
	
	private static final Integer COURSE_ID = 1;
	
	private static final Integer INVALID_COURSE_ID = 8888;
	
	private static final String COURSE_NAME = "some course";
	
	@BeforeEach
	public void setUp() {
		Mockito.when(courseRepository.findById(COURSE_ID)).thenReturn(buildCourse());
		
		Mockito.when(courseRepository.findAll()).thenReturn(buildCourses());
		
		Mockito.when(courseRepository.findAllWithoutStudents()).thenReturn(buildCourses());
		
		Mockito.when(courseRepository.save(any())).thenReturn(buildCourse().get());
	}
	
	@Test
	public void findById_whenInvalidId_thenRaisesResourceNotFoundException() {
		Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
			service.findById(INVALID_COURSE_ID);
		});

		String expectedMessage = "Course not found with id: '8888'";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void findById_happyPaht() {
		CourseDTO courseDTO = service.findById(COURSE_ID);
		assertNotNull(courseDTO);
		
	}
	
	@Test
	public void findByIdWithStudents_whenInvalidId_thenRaisesResourceNotFoundException() {
		Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
			service.findByIdWithStudents(INVALID_COURSE_ID);
		});

		String expectedMessage = "Course not found with id: '8888'";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void findByIdWithStudents_happyPath() {
		CourseWithStudentsDTO courseWithStudentsDTO = service.findByIdWithStudents(COURSE_ID);
		assertNotNull(courseWithStudentsDTO);
		assertEquals(courseWithStudentsDTO.getStudents().size(), 1);
	}
	
	@Test
	public void findAll_returnAllCourses() {
		List<CourseDTO> courseDTOs = service.findAll();
		assertNotNull(courseDTOs);
		assertEquals(2, courseDTOs.size());
	}
	
	@Test
	public void findAllWithoutStudents_returnAllCoursesWithoutStudents() {
		List<CourseDTO> courseDTOs = service.findAll();
		assertNotNull(courseDTOs);
		assertEquals(2, courseDTOs.size());
	}
	
	@Test
	public void save_returnsNewCourse() {
		CourseDTO newCourseDTO = new CourseDTO();
		newCourseDTO.setName(COURSE_NAME);

		CourseDTO savedCourseDTO = service.save(newCourseDTO);

		assertEquals(newCourseDTO.getName(), savedCourseDTO.getName());
	}
	
	@Test
	public void delete_happyPath() {
		assertDoesNotThrow(() -> {
			service.delete(COURSE_ID);
		});
	}
	
	@Test
	public void delete_whenDeletingInvalidCourse_thenRaisesResourceNotFoundException() {
		Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
			service.delete(INVALID_COURSE_ID);
		});

		String expectedMessage = "Course not found with id: '8888'";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	private Optional<Course> buildCourse() {
		Course course = new Course();
		course.setId(1);
		course.setName(COURSE_NAME);
		
		Set<Student> students = new HashSet<Student>();
		students.add(new Student());
		
		course.setStudents(students);
		return Optional.of(course);
	}
	
	private List<Course> buildCourses() {
		List<Course> courses = new ArrayList<Course>();
		courses.add(new Course());
		courses.add(new Course());
		
		return courses;
	}
}
