package com.tiago.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.tiago.entity.Course;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class CourseRepositoryTest {

	@Autowired
	CourseRepository repository;

	@Test
	public void findAllWithoutStudents_whenTwoCoursesDoNotHaveStudents_thenReturnThem() {
		List<Course> courses = repository.findAllWithoutStudents();

		assertEquals(2, courses.size());
	}

}
