package com.tiago.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.tiago.entity.Student;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class StudentRepositoryTest {

	@Autowired
	StudentRepository repository;

	@Test
	public void findAllNotEnrolledToAnyCourse_whenTwoStudentsAreNotEnrolledToAnyCourse_thenReturnThem() {
		List<Student> students = repository.findAllNotEnrolledToAnyCourse();

		assertEquals(2, students.size());
	}

}
